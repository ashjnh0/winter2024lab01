import java.util.Scanner;

public class hangman {
  
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Let's play Hangman!");
        System.out.println("Enter a 4 letter word for another player to guess");
	// player1 will input a word for player2 to guess
        String word = reader.nextLine(); 
	// will clear the screen so that player2 wont see the word to guess
        System.out.print("\033[H\033[2J"); 
        System.out.flush();
	// converts the word to uppercase
        word = word.toUpperCase(); 
	//runs the game
        runGame(word, reader);
    }

  //will loop through the word to check if the letters to guess is in the word
    public static int isLetterInWord(String word, char c) {
        for (int i = 0; i < word.length(); i++) { 
            if (Character.toUpperCase(word.charAt(i)) == Character.toUpperCase(c)) {
                return i; //return the index of the letter if its found in the word
            }
        }
        return -1; //return -1 if its not found in the word
    }
	public static char toUpperCase(char c) {
        return Character.toUpperCase(c);
    }

    public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
        String wordToPrint = "";
	  // this will print the word with the letters that the user has guessed
        if (word.length() >= 1 && letter0) { 
      // it will print the letter in uppercase if the letter is in the word
            wordToPrint += Character.toUpperCase(word.charAt(0));         
        } else {  // if the letter is not found in the word, print _
            wordToPrint += "_"; 
        }

        if (word.length() >= 2 && letter1) {
            wordToPrint += Character.toUpperCase(word.charAt(1));
        } else {
            wordToPrint += "_";
        }

        if (word.length() >= 3 && letter2) {
            wordToPrint += Character.toUpperCase(word.charAt(2));
        } else {
            wordToPrint += "_";
        }

        if (word.length() >= 4 && letter3) {
            wordToPrint += Character.toUpperCase(word.charAt(3));
        } else {
            wordToPrint += "_";
        }
        System.out.println("\nYour result is: " + wordToPrint);
    }

    public static void runGame(String word, Scanner reader) {
      //initialize the variables
        boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;
		
	  //number of guesses the user has
        int count = 6; 
	  // will track if the game is won or not
        boolean gameWon = false; 
      
      // a loop that will run until the user guesses the word or runs out of guesses
        while (count > 0 && !gameWon) {
            System.out.println("\nEnter a letter!\n");
            char c = reader.next().charAt(0);
            int index = isLetterInWord(word, c);

            if (index == -1) {
                count--; // number of guesses goes down if user guesses the wrong word
            } else {
              // if the letter is in the word, the boolean will turn true
                if (index == 0 && !letter0) { 
                    letter0 = true; 
                }
                if (index == 1 && !letter1) {
                    letter1 = true;
                }
                if (index == 2 && !letter2) {
                    letter2 = true;
                }
                if (index == 3 && !letter3) {
                    letter3 = true;
                }
            }
		  // will print the word with the letters that are guessed 
            printWork(word, letter0, letter1, letter2, letter3); 

          // will end the game if user guessed all letters right
            if (letter0 && letter1 && letter2 && letter3) {
                gameWon = true;
                System.out.println("\nYou won the game! :)");
            } else {
              // will print the number of guesses left & game ends if user runs out of guesses
                if (count > 0) {
                    System.out.println("\n--You have " + count + " guesses left--");
                } else {
                    System.out.println("\nYou have no more guesses left. Game over :(");
                }
            }
        }
    }
}

